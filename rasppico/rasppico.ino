#define ADC_RESOLUTION 12
#define SERIAL_BAUDRATE 115200
#define READING_COUNT 100
#define READING_DELAY_US 1
#define LOOP_DELAY_MS 30
#define START_DELAY_MS 5000

const char pinUART1Tx = 4;
const char pinUART1Rx = 5;
const char pinAlarmLed = 15;
const char pinHorizontalPotentiometer = 26;
const char pinVerticalPotentiometer = 27;

bool isAlarmOn = false;

UART SerialBT(pinUART1Tx, pinUART1Rx, 0, 0);

struct Data{
  uint16_t verticalPotentiometer;
  uint16_t horizontalPotentiometer; //= 0;
};

union Dataunion{
  Data data;
  char dataSent[sizeof(Data)];
} unionSent;

int averageADCValue(char adcPin){ // Takes READING_COUNT values and averages
  int readingsSum = 0;
  for (int i = 0; i < 100; i++){
    readingsSum += analogRead(adcPin);
    delayMicroseconds(READING_DELAY_US);
  }
  readingsSum /= 100;
  return readingsSum;
}

void initializeSerial(){
  SerialBT.begin(SERIAL_BAUDRATE);
}

void initializePins(){
  pinMode(pinVerticalPotentiometer, INPUT);
  pinMode(pinHorizontalPotentiometer, INPUT);
}

void initializeADC(){
  analogReadResolution(ADC_RESOLUTION);
}

void readPotentiometers(){
  unionSent.data.verticalPotentiometer = averageADCValue(pinVerticalPotentiometer);
  unionSent.data.horizontalPotentiometer = averageADCValue(pinHorizontalPotentiometer);
}

void sendData(){
  for (unsigned int i = 0; i < sizeof(Data); i++) {
  SerialBT.write(unionSent.dataSent[i]);
  }
}

void readData(){
  if(SerialBT.available()){
    char dataRecieved = SerialBT.read();
    if (dataRecieved == 0x45){
      isAlarmOn = true;
    } else if (dataRecieved == 0x42){
      isAlarmOn = false;
    }
  }
}

void checkAlarm(){
  if(isAlarmOn == true){
    digitalWrite(pinAlarmLed, 1);
  } else {
    digitalWrite(pinAlarmLed, 0);
  }
}

void setup() {
  initializeSerial();
  initializePins();
  initializeADC(); 
  delay(START_DELAY_MS);
}

void loop() {
  readPotentiometers();
  sendData();
  readData();
  checkAlarm();

  delay(LOOP_DELAY_MS);
}