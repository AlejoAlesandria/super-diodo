#include "esp_bt_main.h"
#include "esp_gap_bt_api.h"
#include "esp_spp_api.h"
#include "esp_gap_bt_api.h"
#include <BluetoothSerial.h>

#define PWM_ADC_RESOLUTION 12
#define SERIAL_BAUDRATE 115200
#define READING_DELAY_MS 30
#define MOTOR_PWM 0
#define SERVO_PWM 1
#define SERVO_LOW_LIMIT 140
#define SERVO_HIGH_LIMIT 440
#define DISTANCE_ALARM_CM 50
#define ZERO_THRESHOLD_MIN 2000
#define ZERO_THRESHOLD_MAX 3000
#define ALARM_RESPOND_DELAY_MS 800
#define ALARM_RESPOND_PWM_VALUE 4095
#define ALARM_DISABLED_TIME_MS 1000
#define ULTRASONIC_SENSOR_TIMEOUT 8000
#define ULTRASONIC_CONVERSION_FACTOR 58.27336033

const char pinTriggerFront = 33;
const char pinEchoFront = 25;
const char pinTriggerBack = 26;
const char pinEchoBack = 27;

const char pinEnableA = 19;
const char pinIn1 = 18;
const char pinIn2 = 5;

const char pinServo = 32;

bool isAlarmOnFront = false;
bool isAlarmOnBack = false;
bool alarmAntibounceOn = false;
bool alarmAntibounceOff = false;

unsigned long alarmOnTime = 0;

static esp_err_t connHan;
static uint8_t address[6] = {0x98, 0xD3, 0x31, 0x70, 0x4A, 0xD8};
const char *pin = "1337";

BluetoothSerial SerialBT;

struct Data{
  uint16_t verticalPotentiometer;
  uint16_t horizontalPotentiometer; //= 0;
};

union DataUnion{
  Data data;
  char dataRecieved[sizeof(Data)];
} unionRecieved;

struct UltrasonicSensor{
  char triggerPin;
  char echoPin;
};

UltrasonicSensor frontSensor ={ 
  pinTriggerFront,
  pinEchoFront
};

UltrasonicSensor backSensor ={ 
  pinTriggerBack,
  pinEchoBack
};

void connect(uint8_t *addr){
    esp_spp_init(ESP_SPP_MODE_CB);
    connHan = esp_spp_connect(ESP_SPP_SEC_NONE, ESP_SPP_ROLE_SLAVE, 1, addr);
    //return (connHan == ESP_OK); // Doesn't return false when connection isn't established
}

float calculateDistance(UltrasonicSensor sensor){
  digitalWrite(sensor.triggerPin, 1);
  delayMicroseconds(10);
  digitalWrite(sensor.triggerPin, 0);
  float duration = pulseIn(sensor.echoPin, 1, ULTRASONIC_SENSOR_TIMEOUT);
  float distance = duration / ULTRASONIC_CONVERSION_FACTOR; // d = v*t;

  return distance;
}

void initializeSerialAndBluetooth(){
  SerialBT.begin("ESP32", true);
  SerialBT.setPin(pin);
  connect(address);
}

void initializePins(){
  pinMode(pinTriggerFront, OUTPUT);
  pinMode(pinEchoFront, INPUT);
  pinMode(pinTriggerBack, OUTPUT);
  pinMode(pinEchoBack, INPUT);
  pinMode(pinEnableA, OUTPUT);
  pinMode(pinIn1, OUTPUT);
  pinMode(pinIn2, OUTPUT);
  pinMode(pinServo, OUTPUT);

  ledcSetup(MOTOR_PWM, 50, PWM_ADC_RESOLUTION);
  ledcAttachPin(pinEnableA, MOTOR_PWM);

  ledcSetup(SERVO_PWM, 50, PWM_ADC_RESOLUTION);
  ledcAttachPin(pinServo, SERVO_PWM);

  analogReadResolution(PWM_ADC_RESOLUTION);
}

void readBTSerial(){
  if (SerialBT.available() > 0) {
    SerialBT.readBytes(unionRecieved.dataRecieved, sizeof(Data));
  }
}

void checkSensors(){
  float frontDistance = calculateDistance(frontSensor);
  float backDistance = calculateDistance(backSensor);
  if(frontDistance < DISTANCE_ALARM_CM && frontDistance > 0){
    if(alarmAntibounceOn == false){
      SerialBT.write(0x45);
      alarmAntibounceOn = true;
    }
    alarmAntibounceOff = false;
    isAlarmOnFront = true;
    isAlarmOnBack = false;
    alarmOnTime = millis();
  } else if(backDistance < DISTANCE_ALARM_CM && backDistance > 0){
    if(alarmAntibounceOn == false){
      SerialBT.write(0x45);
      alarmAntibounceOn = true;
    }
    alarmAntibounceOff = false;
    isAlarmOnFront = false;
    isAlarmOnBack = true;
    alarmOnTime = millis();
  } else{
    if(alarmAntibounceOff == false){
      SerialBT.write(0x42);
      alarmAntibounceOff = true;
    }
    alarmAntibounceOn = false;
    isAlarmOnFront = false;
    isAlarmOnBack = false;
  }
}

void brake(){
  digitalWrite(pinIn1, 1);
  digitalWrite(pinIn2, 1);
  ledcWrite(MOTOR_PWM, 4095);
}

void accelerate(){
  if(isAlarmOnFront == false && isAlarmOnBack == false && (millis() - alarmOnTime) > ALARM_DISABLED_TIME_MS){ 
    if(unionRecieved.data.verticalPotentiometer > ZERO_THRESHOLD_MIN && unionRecieved.data.verticalPotentiometer < ZERO_THRESHOLD_MAX){
      brake();
    } else if(unionRecieved.data.verticalPotentiometer > ZERO_THRESHOLD_MAX){
      digitalWrite(pinIn1, 0);
      digitalWrite(pinIn2, 1);
    } else {
      digitalWrite(pinIn1, 1);
      digitalWrite(pinIn2, 0);
    }
    ledcWrite(MOTOR_PWM, abs(unionRecieved.data.verticalPotentiometer - 2048)*2);
  } else{
    if(isAlarmOnFront == true){
      ledcWrite(SERVO_PWM, (SERVO_HIGH_LIMIT+SERVO_LOW_LIMIT)/2);
      brake();
      digitalWrite(pinIn1, 0);
      digitalWrite(pinIn2, 1); 
      ledcWrite(MOTOR_PWM, ALARM_RESPOND_PWM_VALUE);
      delay(ALARM_RESPOND_DELAY_MS);  //CHECK IF DELAY IS NECCESARY
      brake();
    } else if(isAlarmOnBack == true){
      ledcWrite(SERVO_PWM, (SERVO_HIGH_LIMIT+SERVO_LOW_LIMIT)/2);
      brake();
      digitalWrite(pinIn1, 1);
      digitalWrite(pinIn2, 0); 
      ledcWrite(MOTOR_PWM, ALARM_RESPOND_PWM_VALUE);
      delay(ALARM_RESPOND_DELAY_MS);  //CHECK IF DELAY IS NECCESARY
      brake();  
      }
  }
}

void steerServo(){
  if(isAlarmOnFront == false && isAlarmOnBack == false){
    uint16_t duty = map(unionRecieved.data.horizontalPotentiometer, 0, pow(2, PWM_ADC_RESOLUTION) - 1, SERVO_LOW_LIMIT, SERVO_HIGH_LIMIT);
    ledcWrite(SERVO_PWM, duty);
  }
}

void flushBTSerial(){
  int buffer = SerialBT.available();
  if(buffer > 0){
   for(int i = 0; i < buffer; i++){
     SerialBT.read();
   }
  }  
}

void setup() {
  initializePins();
  initializeSerialAndBluetooth();
  unionRecieved.data.horizontalPotentiometer = 2015;
  unionRecieved.data.verticalPotentiometer = 2070;
}

int buffer = 0;

void loop() {
  readBTSerial();
  checkSensors();
  accelerate();
  steerServo();
  flushBTSerial();
  
  delay(READING_DELAY_MS);
}
