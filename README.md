# Super Diodo



## Trabajo práctico final de Sistemas de Comunicaciones. UTN FR San Francisco

En este repositorio se encuentran subidos todos los archivos relacionados al trabajo práctico final de Sistemas de Comunicaciones. Contiene archivos de programación de los microcontroladores, esquemáticos del circuito y de pcb, y variados modelos 3D.

## Autores

- [ ] Cignetti, Mateo
- [ ] Galliano, Ignacio
- [ ] Alesandria, Alejo
